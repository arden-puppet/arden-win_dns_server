# @summary Parameters for the conditional_forwarder_zone type
type Win_dns_server::ConditionalForwarderZone = Struct[
  ensure                      => Enum['present', 'absent'],
  master_servers              => Array[Stdlib::IP::Address::V4::Nosubnet],
  use_recursion               => Boolean,
  Optional[forwarder_timeout] => Integer,
]
