# @summary Global dns server settings 
type Win_dns_server::Settings = Struct[
  disable_recursion     => Boolean,
  round_robin_responses => Boolean,
  enable_debug_logging  => Boolean,
  debug_log_path        => Stdlib::AbsolutePath,
  debug_log_file_size   => Integer,
]
