# win_dns_server

[![pipeline status](https://gitlab.com/arden-puppet/arden-win_dns_server/badges/master/pipeline.svg)](https://gitlab.com/arden-puppet/arden-win_dns_server/commits/master) [![Version](https://img.shields.io/puppetforge/v/arden/win_dns_server.svg)](https://forge.puppet.com/arden/win_dns_server) [![coverage report](https://gitlab.com/arden-puppet/arden-win_dns_server/badges/master/coverage.svg)](https://gitlab.com/arden-puppet/arden-win_dns_server/commits/master)

## Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with win_dns_server](#setup)
    * [What win_dns_server affects](#what-win_dns_server-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with win_dns_server](#beginning-with-win_dns_server)
1. [Usage](#usage)
1. [Limitations](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

This module provides a basic method to manage the configuration of the DNS
server feature on a Windows server instance. While much of the configuration
is accomplished via PowerShell DSC it was necessary to implement a new wrapper
for the currently unsupported Conditional Forwarder Zone types.

## Setup

### Setup Requirements

Pluginsync should be enabled. Additionally, you'll need to perform the
configuration steps detailed for the DSC, pwshlib, and the resource_api
module.

### Beginning with win_dns_server

The following example will configure the DNS service, perform some basic
global settings, and create two forwarder zones.

```yaml
win_dns_server::hash_server_settings:
  debug_log_path: 'C:\temp\dns_debug.log'
  disable_recursion: false
  round_robin_responses: true
win_dns_server::array_publish_addresses:
  - '10.100.100.10'
  - '10.200.100.10'
win_dns_server::hash_forwarder_zones:
  '10.in-addr.arpa':
    master_servers:
      - '172.17.100.10'
      - '172.17.100.11'
    use_recursion: false
```

## Limitations

Currently AD backed DNS servers are only supported in a limited form. You
should not try to create conditional forwarder zones on them!

Also, if the target zone already exists but is of the incorrect type things will go sideways. This is poorly tested at the moment, however, I believe the command will silently fail to create the target zone.

## Development

Make a pull request and we'll figure it out!
