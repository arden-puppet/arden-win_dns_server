# frozen_string_literal: true

require 'spec_helper'
require 'deep_merge'

describe 'win_dns_server::install' do
  it 'compiles' do
    is_expected.to compile
  end

  it 'installs the DNS feature' do
    is_expected.to contain_windowsfeature('DNS').with(
      ensure: 'present',
      installmanagementtools: true,
    )
  end

  ['5.5.18', '6.21.1', '7.5.1'].each do |puppetversion|
    context "on puppet '#{puppetversion}'" do
      let(:facts) do
        {
          puppetversion: puppetversion,
        }
      end

      if puppetversion.match?(%r{^5.5})
        it "includes 'resource_api::install::agent'" do
          is_expected.to contain_class('resource_api::install::agent')
        end
      else
        it "does not include 'resource_api::install::agent'" do
          is_expected.not_to contain_class('resource_api::install::agent')
        end
      end
    end
  end
end
