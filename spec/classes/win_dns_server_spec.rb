# frozen_string_literal: true

require 'spec_helper'
require 'deep_merge'

describe 'win_dns_server' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it 'compiles with default parameters' do
        is_expected.to compile
      end

      it 'contains the correct classes and enforces their ordering' do
        is_expected.to contain_class('win_dns_server::install').with(
          before: ['Class[Win_dns_server::Config]'],
        )
        is_expected.to contain_class('win_dns_server::config').with(
          notify: ['Class[Win_dns_server::Service]'],
        )
        is_expected.to contain_class('win_dns_server::service')
      end
    end
  end
end
