# frozen_string_literal: true

require 'spec_helper'
require 'deep_merge'

describe 'win_dns_server::service' do
  it 'compiles' do
    is_expected.to compile
  end

  it 'enables the DNS service' do
    is_expected.to contain_service('DNS').with(
      ensure: 'running',
      enable: true,
    )
  end
end
