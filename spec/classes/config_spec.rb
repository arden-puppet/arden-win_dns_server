# frozen_string_literal: true

require 'spec_helper'
require 'deep_merge'

describe 'win_dns_server::config' do
  ss_logging_enabled = {
    'disable_recursion' => false,
    'round_robin_responses' => true,
    'enable_debug_logging' => true,
    'debug_log_path' => 'C:\Temp\dns_debug.log',
    'debug_log_file_size' => 134_217_728,
  }
  ss_logging_disabled = {
    'disable_recursion' => false,
    'round_robin_responses' => true,
    'enable_debug_logging' => false,
    'debug_log_path' => 'C:\Temp\dns_debug.log',
    'debug_log_file_size' => 134_217_728,
  }
  publish_addresses = [
    '172.28.25.25',
    '10.255.0.1',
  ]
  loglevel_enabled = 24_833
  loglevel_disabled = 0
  conditional_forwarder_zones = {
    '10.in-addr.arpa' => {
      'ensure' => 'present',
      'master_servers' => [ '10.100.100.1', '10.200.200.1' ],
      'use_recursion' => true,
      'forwarder_timeout' => 3,
    }
  }

  let(:facts) do
    {
      networking: {
        ip: '172.24.25.21'
      },
    }
  end
  let(:params) do
    {
      array_publish_addresses: [],
      hash_server_settings: ss_logging_enabled,
      hash_forwarder_zones: conditional_forwarder_zones,
    }
  end

  it 'compiles' do
    is_expected.to compile
  end

  it 'publishes its primary IP' do
    is_expected.to contain_dsc_registry('reg_dns_limit-self-publish-addresses').with(
      dsc_ensure: 'Present',
      dsc_key: 'HKLM:\SYSTEM\CurrentControlSet\Services\DNS\Parameters',
      dsc_valuename: 'PublishAddresses',
      dsc_valuedata: facts[:networking][:ip].to_s,
      dsc_valuetype: 'String',
    )
  end

  it 'creates the forwarder zones' do
    is_expected.to contain_conditional_forwarder_zone('10.in-addr.arpa').with(
      ensure: 'present',
      master_servers: ['10.100.100.1', '10.200.200.1'],
      use_recursion: true,
      forwarder_timeout: 3,
    )
  end

  it 'configures the core service' do
    is_expected.to contain_dsc_xdnsserversetting('dns_server_settings').with(
      dsc_name: 'dns_server_settings',
      dsc_roundrobin: ss_logging_enabled['round_robin_responses'],
      dsc_norecursion: ss_logging_enabled['disable_recursion'],
      dsc_logfilemaxsize: ss_logging_enabled['debug_log_file_size'],
      dsc_logfilepath: ss_logging_enabled['debug_log_path'],
      dsc_loglevel: loglevel_enabled,
    )
  end

  context 'without logging' do
    before(:each) do
      params.deep_merge!(
        {
          array_publish_addresses: [],
          hash_server_settings: ss_logging_disabled,
        },
      )
    end

    it 'the core service disables logging' do
      is_expected.to contain_dsc_xdnsserversetting('dns_server_settings').with(
        dsc_name: 'dns_server_settings',
        dsc_roundrobin: ss_logging_disabled['round_robin_responses'],
        dsc_norecursion: ss_logging_disabled['disable_recursion'],
        dsc_loglevel: loglevel_disabled,
      )
    end
  end

  context 'with a publish address list' do
    before(:each) do
      params.deep_merge!(
        {
          array_publish_addresses: publish_addresses,
          hash_server_settings: ss_logging_enabled,
        },
      )
    end

    it 'overrides the publish address value' do
      is_expected.to contain_dsc_registry('reg_dns_limit-self-publish-addresses').with(
        dsc_ensure: 'Present',
        dsc_key: 'HKLM:\SYSTEM\CurrentControlSet\Services\DNS\Parameters',
        dsc_valuename: 'PublishAddresses',
        dsc_valuedata: publish_addresses.join(' '),
        dsc_valuetype: 'String',
      )
    end
  end
end
