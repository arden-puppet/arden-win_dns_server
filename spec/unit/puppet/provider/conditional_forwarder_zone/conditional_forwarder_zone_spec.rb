require 'spec_helper'

# TODO: cleanup/helper to avoid this spurious declaration
# This allows us to reference the subcomponent without errors later
# somehow related to the way puppet loads providers
module Puppet::Provider::ConditionalForwarderZone; end
require 'puppet/provider/conditional_forwarder_zone/conditional_forwarder_zone'

master_servers_argument_tests = [
  {
    value: [ '10.10.10.10', '10.20.10.10' ],
    command: '-MasterServers @("10.10.10.10", "10.20.10.10")',
  },
  {
    value: [ '10.0.5.5' ],
    command: '-MasterServers @("10.0.5.5")',
  },
]

use_recursion_to_argument_tests = [
  {
    value: true,
    command: '-UseRecursion:$true',
  },
  {
    value: false,
    command: '-UseRecursion:$false',
  },
]

forwarder_timeout_to_argument_tests = [
  {
    value: 5,
    command: '-ForwarderTimeout 5',
  },
  {
    value: 15,
    command: '-ForwarderTimeout 15',
  },
]

zone_parse_tests = [
  {
    raw_data: <<~HEREDOC,


      ZoneName         : 10.in-addr.arpa
      UseRecursion     : True
      ForwarderTimeout : 5
      MasterServers    : 10.255.0.1




      HEREDOC
    result: {
      name: '10.in-addr.arpa',
      ensure: 'present',
      master_servers: [ '10.255.0.1' ],
      forwarder_timeout: 5,
      use_recursion: true,
    },
  },
  {
    raw_data: <<~HEREDOC,



      ZoneName         : 16.172.in-addr.arpa
      UseRecursion     : False
      ForwarderTimeout : 3
      MasterServers    : {10.255.0.1, 10.255.128.1}




      HEREDOC
    result: {
      name: '16.172.in-addr.arpa',
      ensure: 'present',
      master_servers: [ '10.255.0.1', '10.255.128.1' ],
      forwarder_timeout: 3,
      use_recursion: false,
    },
  },
]

# Build test data for initial instance parsing
raw_zone_data = <<~HEREDOC.gsub("\n", "\r\n")



ZoneName         : 10.in-addr.arpa
UseRecursion     : False
ForwarderTimeout : 3
MasterServers    : {10.100.0.1, 10.200.0.1}

ZoneName         : 16.172.in-addr.arpa
UseRecursion     : False
ForwarderTimeout : 3
MasterServers    : {10.100.0.1, 10.200.0.1}

ZoneName         : example.org
UseRecursion     : False
ForwarderTimeout : 3
MasterServers    : {10.100.0.1, 10.200.0.1}




HEREDOC

split_zone_data = [
  <<~HEREDOC.strip,
  ZoneName         : 10.in-addr.arpa
  UseRecursion     : False
  ForwarderTimeout : 3
  MasterServers    : {10.100.0.1, 10.200.0.1}
  HEREDOC
  <<~HEREDOC.strip,
  ZoneName         : 16.172.in-addr.arpa
  UseRecursion     : False
  ForwarderTimeout : 3
  MasterServers    : {10.100.0.1, 10.200.0.1}
  HEREDOC
  <<~HEREDOC.strip,
  ZoneName         : example.org
  UseRecursion     : False
  ForwarderTimeout : 3
  MasterServers    : {10.100.0.1, 10.200.0.1}
  HEREDOC
]

base_zone_data = {
  ensure: 'present',
  master_servers: [ '10.100.0.1', '10.200.0.1' ],
  forwarder_timeout: 3,
  use_recursion: false,
}

zones_data = [
  {
    raw_data: split_zone_data[0],
    result: base_zone_data.merge({ name: '10.in-addr.arpa' }),
  },
  {
    raw_data: split_zone_data[1],
    result: base_zone_data.merge({ name: '16.172.in-addr.arpa' }),
  },
  {
    raw_data: split_zone_data[2],
    result: base_zone_data.merge({ name: 'example.org' }),
  },
]

get_process_output = zones_data.map { |e| e[:result] }

# Test the initialiaztion of the resource
get_existing_zones = 'Get-DnsServerZone | Where-Object -Property ZoneType -EQ \'Forwarder\' | Select-Object -Property ZoneName,UseRecursion,ForwarderTimeout,MasterServers | Format-List | Out-String'

# Build a test commands for updating, creating, and deleting a zone
set_zone_master_servers = "Set-DnsServerConditionalForwarderZone \
-Name '10.in-addr.arpa' -MasterServers @(\"10.200.0.1\", \"10.100.0.1\")"
create_zone_cmd = "Add-DnsServerConditionalForwarderZone -Name '10.in-addr.arpa' -UseRecursion:$false -ForwarderTimeout 3 -MasterServers @(\"10.100.0.1\", \"10.200.0.1\")"
delete_zone_cmd = "Remove-DnsServerZone -Name '10.in-addr.arpa' -Force"

# Based on
# https://github.com/puppetlabs/puppetlabs-hue/blob/master/lib/puppet/provider/hue_light/hue_light.rb
# https://github.com/puppetlabs/puppetlabs-hue/blob/master/spec/unit/puppet/provider/hue_light/hue_light_spec.rb
RSpec.describe Puppet::Provider::ConditionalForwarderZone::ConditionalForwarderZone do
  subject(:provider) { described_class.new }

  let(:context) { instance_double('Puppet::ResourceApi::BaseContext', 'context') }
  let(:ps) { instance_double('Pwsh::Manager', 'ps') }

  before(:each) do
    allow(context).to receive(:debug)
    allow(context).to receive(:err)
    allow(context).to receive(:processed)
    allow(context).to receive(:deleted)
    allow(context).to receive(:creating)
    allow(context).to receive(:deleting)
    allow(context).to receive(:updating)
    allow(Pwsh::Manager).to receive(:windows_powershell_supported?).and_return(true)
    allow(Pwsh::Manager).to receive(:instance).and_return(ps)
  end

  describe '#master_servers_to_argument' do
    master_servers_argument_tests.each do |entry|
      it 'constructs the master_servers powershell argument' do
        expect(provider.master_servers_to_argument(entry[:value])).to eq(entry[:command])
      end
    end
  end

  describe '#use_recursion_to_argument' do
    use_recursion_to_argument_tests.each do |entry|
      it 'constructs the UseRecursion powershell argument' do
        expect(provider.use_recursion_to_argument(entry[:value])).to eq(entry[:command])
      end
    end
  end

  describe '#forwarder_timeout_to_argument' do
    forwarder_timeout_to_argument_tests.each do |entry|
      it 'constructs the UseRecursion powershell argument' do
        expect(provider.forwarder_timeout_to_argument(entry[:value])).to eq(entry[:command])
      end
    end
  end

  describe '#parse_conditional_forwarder_zone' do
    zone_parse_tests.each do |entry|
      result = entry[:result]
      raw_data = entry[:raw_data]
      it 'returns a formatted instance' do
        expect(provider.parse_conditional_forwarder_zone(raw_data)).to eq(result)
      end
    end
  end

  context 'runtime' do
    describe '#get_existing_forwarder_zones' do
      before(:each) do
        allow(ps).to receive(:execute) do |arg|
          raise "Unknown command: '#{arg}'" unless arg == get_existing_zones
          {
            stdout: raw_zone_data,
            native_out: '',
            stderr: [],
            exit_code: 0,
          }
        end
      end

      it 'returns an array of zone data' do
        expect(ps).to receive(:execute).with(get_existing_zones)
        expect(provider.get_existing_forwarder_zones(context)).to eq(split_zone_data.map { |e| e.gsub("\n", "\r\n") })
      end
    end

    describe '#get' do
      let(:zones_data) { zones_data }
      let(:zone_commands) do
        command_hash_get_zones.merge(
          {
            get_existing_zones => raw_zone_data,
          },
        )
      end

      before(:each) do
        allow(ps).to receive(:execute) do |arg|
          raise "Unknown command: '#{arg}'" unless arg == get_existing_zones
          {
            stdout: raw_zone_data,
            native_out: '',
            stderr: [],
            exit_code: 0,
          }
        end
      end

      it 'retrieves properties for each zone' do
        expect(ps).to receive(:execute).with(get_existing_zones)
        expect(provider.get(context)).to eq(get_process_output)
      end
    end

    describe '#update_conditional_forwarder_zone' do
      before(:each) do
        allow(ps).to receive(:execute) do |arg|
          raise "Unknown command: '#{arg}'" unless arg == set_zone_master_servers
          {
            stdout: '',
            native_out: '',
            stderr: [],
            exit_code: 0,
          }
        end
      end
      let(:base_zone_data) { base_zone_data }
      let(:set_zone_master_servers) { set_zone_master_servers }

      it 'does nothing when no change is detected' do
        zone_name = '10.in-addr.arpa'
        change = {
          is: base_zone_data,
          should: base_zone_data,
        }
        expect(ps).not_to receive(:execute)
        provider.update_conditional_forwarder_zone(zone_name, change, context)
      end

      it 'updates only the changed value' do
        zone_name = '10.in-addr.arpa'
        should = Marshal.load(Marshal.dump(base_zone_data))
        should[:master_servers] = ['10.200.0.1', '10.100.0.1']
        change = {
          is: base_zone_data,
          should: should,
        }
        expect(ps).to receive(:execute).with(set_zone_master_servers)
        provider.update_conditional_forwarder_zone(zone_name, change, context)
      end
    end

    describe '#create_conditional_forwarder_zone' do
      let(:base_zone_data) { base_zone_data }
      let(:create_zone_cmd) { create_zone_cmd }
      let(:zone_name) { '10.in-addr.arpa' }

      before(:each) do
        allow(ps).to receive(:execute) do |arg|
          raise "Unknown command: '#{arg}'" unless arg == create_zone_cmd
          {
            stdout: '',
            native_out: '',
            stderr: [],
            exit_code: 0,
          }
        end
      end

      it 'raises an error when master_servers is missing' do
        should = Marshal.load(Marshal.dump(base_zone_data))
        should.delete(:master_servers)
        change = { should: should }
        expect { provider.create_conditional_forwarder_zone(zone_name, change, context) }.to raise_error('master_servers is mandatory!')
      end

      it 'creates with all arguments' do
        change = {
          should: base_zone_data,
        }
        expect(ps).to receive(:execute).with(create_zone_cmd)
        provider.create_conditional_forwarder_zone(zone_name, change, context)
      end
    end

    describe '#delete_conditional_forwarder_zone' do
      let(:delete_zone_cmd) { delete_zone_cmd }
      let(:zone_name) { '10.in-addr.arpa' }

      before(:each) do
        allow(ps).to receive(:execute) do |arg|
          raise "Unknown command: '#{arg}'" unless arg == delete_zone_cmd
          {
            stdout: '',
            native_out: '',
            stderr: [],
            exit_code: 0,
          }
        end
      end

      it 'deletes the target zone' do
        expect(ps).to receive(:execute).with(delete_zone_cmd)
        provider.delete_conditional_forwarder_zone(zone_name, context)
      end
    end

    describe '#set' do
      let(:changes) do
        {
          '10.in-addr.arpa' => {
            is: base_zone_data,
          },
          '16.172.in-addr.arpa' => {
            should: base_zone_data
          },
          '17.172.in-addr.arpa' => {
            is: {
              ensure: 'absent',
              title: '17.172.in-addr.arpa',
            },
            should: base_zone_data
          },
          '18.172.in-addr.arpa' => {
            is: base_zone_data,
            should: {
              ensure: 'absent',
              title: '18.172.in-addr.arpa',
            },
          },
          '19.172.in-addr.arpa' => {
            is: {
              ensure: :present,
              name: '19.172.in-addr.arpa',
              use_recursion: false,
              master_servers: ['172.16.1.1'],
            },
            should: {
              ensure: 'present',
              name: '19.172.in-addr.arpa',
              use_recursion: true,
              master_servers: ['172.16.1.1'],
            },
          },
          'example.org' => {
            is: base_zone_data,
            should: {
              ensure: 'present',
              master_servers: [ '10.200.0.1', '10.100.0.1' ],
              forwarder_timeout: 3,
              use_recursion: false,
            }
          }
        }
      end

      it 'performs the correct action for each input zone' do
        expect(context).to receive(:deleting).with('10.in-addr.arpa')
        expect(context).to receive(:deleting).with('18.172.in-addr.arpa')
        expect(context).to receive(:creating).with('16.172.in-addr.arpa')
        expect(context).to receive(:creating).with('17.172.in-addr.arpa')
        expect(context).to receive(:updating).with('example.org')
        expect(context).to receive(:updating).with('19.172.in-addr.arpa')
        provider.set(context, changes)
      end
    end
  end
end
