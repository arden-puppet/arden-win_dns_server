# frozen_string_literal: true

require 'pathname'
require 'base64'
require 'pp'

require 'puppet/resource_api/simple_provider'

begin
  require 'ruby-pwsh'
rescue LoadError
  raise 'Could not load the "ruby-pwsh" library; is the dependency module puppetlabs-pwshlib installed in this environment?'
end

# Implemented using the https://github.com/puppetlabs/puppet-resource_api
# framework
class Puppet::Provider::ConditionalForwarderZone::ConditionalForwarderZone
  # Retrieves the full list of existing ConditionalForwarderZones
  def get(context)
    zone_list = get_existing_forwarder_zones(context)

    context.debug("\nzone name list:\n\t#{zone_list}")

    zones = []
    zone_list.each do |zone_raw|
      zone = parse_conditional_forwarder_zone(zone_raw)
      context.debug(zone[:name], zone.to_s)
      zones.push(zone)
    end

    # Return the resulting zones array
    context.debug("\nparsed zones:\n\t#{zones}")
    zones
  end

  def set(context, changes)
    processing_hash = {}
    context.debug("#set changes:\n\t#{changes}")
    changes.each do |name, change|
      action = if change.key?(:is) && change.key?(:should)
                 is_ensure = change[:is][:ensure].to_s
                 should_ensure = change[:should][:ensure].to_s
                 if is_ensure == 'absent' && should_ensure == 'present'
                   'create'
                 elsif is_ensure == 'present' && should_ensure == 'absent'
                   'delete'
                 elsif is_ensure == 'present' && should_ensure == 'present'
                   'update'
                 else
                   'noop'
                 end
               elsif change.key?(:should)
                 'create'
               else
                 'delete'
               end
      processing_hash[name] = {
        action: action,
        change: change,
      }
    end

    context.debug("#set processing_hash:\n\t#{processing_hash}")
    # Process creation
    processing_hash.each do |name, data|
      context.debug(name, "set: #{data}")
      case data[:action]
      when 'create'
        context.creating(name) do
          create_conditional_forwarder_zone(name, data[:change], context)
        end
      when 'delete'
        context.deleting(name) do
          delete_conditional_forwarder_zone(name, context)
        end
      when 'update'
        context.updating(name) do
          update_conditional_forwarder_zone(name, data[:change], context)
        end
      end
    end

    nil
  end

  # Retrieves an instance of Pwsh::Manager
  def ps_manager
    Pwsh::Manager.instance(Pwsh::Manager.powershell_path, Pwsh::Manager.powershell_args)
  end

  # Retrieve the full list of existing forwarder zones
  def get_existing_forwarder_zones(context)
    # TODO: this may not work if there are too many master servers
    # as powershell may abbreviate them.
    cmd = [
      'Get-DnsServerZone',
      "| Where-Object -Property ZoneType -EQ 'Forwarder'",
      '| Select-Object -Property ZoneName,UseRecursion,ForwarderTimeout,MasterServers',
      '| Format-List',
      '| Out-String',
    ].join(' ')
    context.debug("Executing: { '#{cmd}' }")

    result = run_command(cmd, context)

    # Each zone name should be returned on its own line
    result.strip.split(%r|[\n\r]{4,}|)
  end

  # Create the target zone
  def create_conditional_forwarder_zone(zone_name, change, context)
    should = change[:should]

    # master servers is mandatory
    unless should.key?(:master_servers)
      raise 'master_servers is mandatory!'
    end

    cmd = [
      'Add-DnsServerConditionalForwarderZone',
      "-Name '#{zone_name}'",
      use_recursion_to_argument(should[:use_recursion]),
      forwarder_timeout_to_argument(should[:forwarder_timeout]),
      master_servers_to_argument(should[:master_servers]),
    ].join(' ')
    context.debug(zone_name, "Executing: { '#{cmd}' }")

    result = run_command(cmd, context)

    # Return the result
    raise 'Creation failed! see the debug log!' unless result.exitstatus.zero?
    context.processed(zone_name, nil, should)
  end

  # Delete the target zone
  def delete_conditional_forwarder_zone(zone_name, context)
    cmd = [
      'Remove-DnsServerZone',
      "-Name '#{zone_name}' -Force",
    ].join(' ')
    context.debug(zone_name, "Executing: { '#{cmd}' }")

    result = run_command(cmd, context)

    # Return the result
    raise 'Deletion failed! see the debug log!' unless result.exitstatus.zero?
    context.deleted(zone_name, message: 'Deleted')
  end

  # Update the target zone
  def update_conditional_forwarder_zone(zone_name, change, context)
    changes = []
    is = change[:is]
    should = change[:should]

    should.each do |key, value|
      old_value = is[key]
      case key
      when :master_servers
        if old_value != value
          changes.push({ key: key, arg: master_servers_to_argument(value), old: old_value, new: value })
        end
      when :use_recursion
        if old_value != value
          changes.push({ key: key, arg: use_recursion_to_argument(value), old: old_value, new: value })
        end
      when :forwarder_timeout
        if old_value != value
          changes.push({ key: key, arg: forwarder_timeout_to_argument(value), old: old_value, new: value })
        end
      end
    end

    unless changes.empty?
      cmd = ([
        'Set-DnsServerConditionalForwarderZone',
        "-Name '#{zone_name}'",
      ] + changes.map { |x| x[:arg] }).join(' ')
      context.debug(zone_name, "Executing: { '#{cmd}' }")
      # TODO: do something with the other data in changes

      result = run_command(cmd, context)

      # Return the result
      raise 'Update failed! see the debug log!' unless result.exitstatus.zero?
      context.processed(zone_name, is, should)
    end

    nil
  end

  def master_servers_to_argument(value)
    "-MasterServers @#{value.to_s.tr('[', '(').tr(']', ')')}"
  end

  def use_recursion_to_argument(value)
    "-UseRecursion:$#{value}"
  end

  def forwarder_timeout_to_argument(value)
    "-ForwarderTimeout #{value}"
  end

  # Parses a single conditional forwarder zone into a new hash
  def parse_conditional_forwarder_zone(raw_data)
    # Return the current value or a blank
    zone_properties = {}
    # Attempt to split the properties of the zone
    lines = raw_data.strip.split(%r{\n+})
    lines.each do |line|
      line_parts = line.strip.split(%r{\s+:\s+})
      property_name = line_parts[0]
      property_value = case property_name
                       when 'ZoneName'
                         line_parts[1].strip
                       when 'UseRecursion'
                         line_parts[1].casecmp('true').zero?
                       when 'ForwarderTimeout'
                         line_parts[1].to_i
                       when 'MasterServers'
                         line_parts[1].gsub(%r|[{}]|, '').split(%r{\s*,\s*}).map { |e| e.strip }
                       end

      zone_properties[property_name] = property_value
    end

    # Return the output
    {
      name: zone_properties['ZoneName'],
      ensure: 'present',
      master_servers: zone_properties['MasterServers'],
      forwarder_timeout: zone_properties['ForwarderTimeout'],
      use_recursion: zone_properties['UseRecursion'],
    }
  end

  # Executes the provided command with some sane defaults.
  def run_command(cmd, context)
    # Make sure we can actually run this program
    unless Pwsh::Manager.windows_powershell_supported?
      context.err('Unsupported combination of Windows / PowerShell versions!')
      return
    end

    # Execute the command - based on the execute_resource method here:
    # https://github.com/puppetlabs/puppetlabs-powershell/blob/main/lib/puppet/provider/exec/powershell.rb
    ps         = ps_manager
    result     = ps.execute(cmd)
    stdout     = result[:stdout]
    native_out = result[:native_stdout]
    stderr     = result[:stderr]
    exit_code  = result[:exit_code]

    # Output error messages
    stderr&.each { |e| context.debug("STDERR: #{e.chop}") unless e.empty? }

    context.debug("STDERR: #{result[:errormessage]}") unless result[:errormessage].nil?

    # Parse and return the result
    Puppet::Util::Execution::ProcessOutput.new(stdout.to_s + native_out.to_s, exit_code.nil? ? 0 : exit_code)
  end
end
