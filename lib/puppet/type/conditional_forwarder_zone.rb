# frozen_string_literal: true

require 'puppet/resource_api'

Puppet::ResourceApi.register_type(
  name: 'conditional_forwarder_zone',
  desc: <<-EOS,
    This type manages Conditional Forwarder Zones on a Windows DnsServer.

    Examples:

    ```puppet
    conditional_forwarder_zone { 'example.org':
      master_servers    => [ '10.0.0.1', '20.0.0.1' ],
      use_recursion     => true,
      forwarder_timeout => 5,
    }

    # Define a conditional forwarder zone for 10.0.0.0/8
    conditional_forwarder_zone { '10.in-addr.arpa':
      master_servers    => [ '10.0.0.10', '10.0.0.20' ],
      use_recursion     => false,
      forwarder_timeout => 5,
    }
    ```


  EOS
  attributes: {
    ensure: {
      type: 'Enum[present, absent]',
      desc: 'Whether this conditional forwarder zone should be presesnt or absent on the target node.'
    },
    name: {
      type:      'Pattern[/\A(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])\z/]',
      behaviour: :namevar,
      desc:      "The fully qualified DNS zone name. This includes both forward lookup zones like 'example.org' and reverse lookup zones like '48.0.10.in-addr.arpa'",
    },
    use_recursion: {
      type:    'Boolean',
      default: 'false',
      desc:    'When true the DNS server will perform recursive queries to resolve names in this zone if it does not receive a response before the forward timeout.'
    },
    forwarder_timeout: {
      type:    'Integer[0,15]',
      default: 3,
      desc:    'The the maximum amount of time, in seconds, that this server will wait for a response from one of the forwarder targets.'
    },
    # how do we make this mandatory?
    master_servers: {
      type: 'Array[Pattern[/\A([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])){3}\z/]]',
      desc: 'An array of servers which are authoratative for this DNS zone. These *must* be specificed by IP.'
    },
  },
)
