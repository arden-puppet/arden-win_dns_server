# @summary Internal class which configures the DNS service
class win_dns_server::service {
  # Start the DNS service
  service { 'DNS':
    ensure => 'running',
    enable => true,
  }
}
