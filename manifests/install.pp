# @summary Adds the Windows DNS Server feature
class win_dns_server::install {
  # Ensure the DNS feature is installed
  Windowsfeature { 'DNS':
    ensure                 => 'present',
    installmanagementtools => true,
  }

  if $facts['puppetversion'] =~ /^5.5/ {
    include resource_api::install::agent
  }
}
