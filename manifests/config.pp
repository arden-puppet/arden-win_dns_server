# @param array_publish_addresses
#   See main class documentation.
#
# @summary Performs zone creation and update operations. Internal class
class win_dns_server::config (
  Array[Stdlib::IP::Address::V4::Nosubnet] $array_publish_addresses                  = $win_dns_server::array_publish_addresses,
  Win_dns_server::Settings                 $hash_server_settings                     = $win_dns_server::hash_server_settings,
  Hash[Stdlib::Fqdn,Win_dns_server::ConditionalForwarderZone]  $hash_forwarder_zones = $win_dns_server::hash_forwarder_zones,
) {
  # Configure DNS self-registration such that only our primary IP is listed or,
  # when specified a list of desired IPs
  if empty($array_publish_addresses) {
    $publish_address_string = $facts['networking']['ip']
  } else {
    $publish_address_string = join($array_publish_addresses, ' ')
  }
  dsc_registry { 'reg_dns_limit-self-publish-addresses':
    dsc_ensure    => 'Present',
    dsc_key       => 'HKLM:\SYSTEM\CurrentControlSet\Services\DNS\Parameters',
    dsc_valuename => 'PublishAddresses',
    dsc_valuedata => $publish_address_string,
    dsc_valuetype => 'String',
  }

  # Configure the DNS service
  # DNS Server log level
  # http://www.systemmanager.ru/win2k_regestry.en/46746.htm
  if $hash_server_settings['enable_debug_logging'] {
    # 0x2000 - recieve (not send)
    # 0x4000 - UDP packets
    # 0x0100 - Questions (as opposed to answers)
    # 0x0001 - Log queries
    $log_level_value = '0x00006101'
  } else {
    $log_level_value = '0x00000000'
  }
  # This should be tweaking registry parameter LogLevel in key
  # 'HKLM:\SYSTEM\CurrentControlSet\Services\DNS\Parameters\LogLevel',

  dsc_xdnsserversetting { 'dns_server_settings':
    dsc_name           => 'dns_server_settings',
    dsc_roundrobin     => $hash_server_settings['round_robin_responses'],
    dsc_norecursion    => $hash_server_settings['disable_recursion'],
    dsc_logfilemaxsize => $hash_server_settings['debug_log_file_size'],
    dsc_logfilepath    => $hash_server_settings['debug_log_path'],
    dsc_loglevel       => Integer($log_level_value, 16),
  }

  $hash_forwarder_zones.each |$zone_name, $zone_data| {
    conditional_forwarder_zone { $zone_name:
      *      => $zone_data
    }
  }
}
