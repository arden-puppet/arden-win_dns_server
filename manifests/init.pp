# @param hash_server_settings
#   THis hash configures global DNS service settings when specified. These
#   settings are derived from the
#   [xDnsServer](https://github.com/puppetlabs/puppetlabs-dsc/blob/master/types.md#xdnsserver)
#   module
#
# @param array_publish_addresses
#   Optional array listing the addresses this DNS server should self publish. By
#   default the DNS server will publish only it's primary IP.
#
# @param hash_forwarder_zones
#   A hash detailing the conditional forwarding DNS zones which will be created
#   on this server. Note that the key to the hash is used as the zone name and
#   must be specified.
#
# @summary Configures the Windows DNS Server service on a given node.
class win_dns_server (
  Win_dns_server::Settings                                              $hash_server_settings,
  Optional[Array[Stdlib::IP::Address::V4::Nosubnet]]                    $array_publish_addresses = [],
  Optional[Hash[Stdlib::Fqdn,Win_dns_server::ConditionalForwarderZone]] $hash_forwarder_zones    = {},
) {
  contain 'win_dns_server::install'
  contain 'win_dns_server::config'
  contain 'win_dns_server::service'

  Class['win_dns_server::install']
  -> Class['win_dns_server::config']
  ~> Class['win_dns_server::service']
}
